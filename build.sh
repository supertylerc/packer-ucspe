#!/usr/bin/env bash -e
echo "THERE IS ONLY ZUUL."
DIR=$(dirname "$0")
UCSPE_IMAGE="${UCSPE_IMAGE:-UCSPE_3.1.2e.ova}"
UCSPE_IMAGE="${1:-$UCSPE_IMAGE}"

# path to binaries, override with env vars if necessary
PACKER_PATH="${PACKER_PATH:-$(which packer)}"
VAGRANT_PATH="${VAGRANT_PATH:-$(which vagrant)}"

# commands to build and add the Vagrant box
PACKER_COMMAND=(${PACKER_PATH} build -var image=${UCSPE_IMAGE} packer-ucspe.json)
VAGRANT_COMMAND=(${VAGRANT_PATH} box add ${DIR}/builds/virtualbox-ucspe.box --name supertylerc/ucspe)

# build the box
echo "Building box.  Please wait!"
"${PACKER_COMMAND[@]}"
echo "Dabu."

# add the box to Vagrant
echo "Adding box to Vagrant.  Please wait!"
"${VAGRANT_COMMAND[@]}"
echo "Zug zug."

# clean up artifacts
echo "Removing artifacts.  Please wait!"
rm -rf ${DIR}/{builds,packer_cache}
echo -e "Work complete.\n"
echo "You can now run UCS PE with the following command:"
echo -e "  vagrant init supertylerc/ucspe && vagrant up\n"
echo "Note: the 'vagrant up' command will likely report an error, but it's okay."
echo "Note: you can visit the web UI at http://127.0.0.1:8080"
echo "Note: the web UI can take a long time to load.  Please be patient."
