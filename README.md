# packer-ucspe

Convert the UCS PE VM from an OVA to a Vagrant box using Packer.

## Requirements

- [Packer](https://www.packer.io/)
- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](https://www.virtualbox.org/)
- [Cisco UCS PE](https://communities.cisco.com/docs/DOC-71877)

Note: you must download and provide the UCS PE OVA.  It will not
be provided here for licensing reasons.

## Usage

- Clone this repository and cd to the directory
- Copy the OVA to this directory
- Run `./build.sh`
- Wait patiently

Note: if your OVA name is something other than `UCSPE_3.1.2e.ova`,
you must provide it as a parameter to `build.sh`, like this:
`./build.sh UCSPE_2.5.2a.ova`.  Note that although this should
work, it hasn't been explicitly tested.

Note: do not use the included `Vagrantfile`.  It is here as a template
for Packer.  Simply follow the instructions from the output of the
build script.

## Modifying Behavior

You can modify the image used to build your Vagrant box by passing it
as an argument (as shown above) or by setting the `UCSPE_IMAGE` variable.
Example:

```
# Changing the image name
./build.sh UCSPE_2.5.2a.ova

# OR
UCSPE_IMAGE=UCSPE_2.5.2a.ova ./build.sh

# OR

export UCSPE_IMAGE=UCSPE_2.5.2a.ova
./build.sh
```

Note: the image _must_ be in the same directory as the `build.sh`
script and `Vagrantfile`.

You can also customize the path to Vagrant and/or Packer if necessary.
The script attempts to find it for you, but you may need to specify
the path to the binary manually.  This can be done as below:

```
# Change the path to Packer
PACKER_PATH=/opt/packer/bin/packer ./build.sh

# Change the path to Vagrant
VAGRANT_PATH=/opt/vagrant/bin/vagrant ./build.sh
```

## Starting and Using UCS PE

Once you've built the Vagrant box, you can simply move to a different directory
(it can't be this one) and:

```
vagrant init supertylerc/ucspe && vagrant up
```

Wait a few minutes and then open your browser to `http://127.0.0.1:8080`.
If you need access to the UCS PE console, simply `vagrant ssh`.

## Contributors

- Tyler Christiansen (@supertylerc)

## Thanks

Thanks to Cisco for making UCS PE available for free.
Thanks to HashiCorp for making Packer and Vagrant.
Thanks to Oracle for VirtualBox.
